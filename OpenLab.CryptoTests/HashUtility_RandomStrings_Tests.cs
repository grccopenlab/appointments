﻿using System;
using System.Collections.Generic;

using FluentAssertions;
using NUnit.Framework;

namespace OpenLab.CryptoTests
{
    [TestFixture]
    public class HashUtility_RandomStrings_Tests
    {
        [Test]
        public void GenerateSalt_Should_Not_Duplicate_Salt()
        {
            int iterations = 1000000;

            HashSet<string> set = new HashSet<string>();

            for (int i = 0; i < iterations; i++)
            {
                if (!set.Add(OpenLab.Crypto.HashUtility.GenerateSalt()))
                {
                    break;
                }
            }

            set.Count.ShouldBeEquivalentTo<int>(iterations, "HashUtility should generate the specified number of random salt values without duplicates.");
        }


        [Test]
        public void GenerateToken_Should_Not_Duplicate_Token()
        {
            int iterations = 1000000;

            HashSet<string> set = new HashSet<string>();

            for (int i = 0; i < iterations; i++)
            {
                if (!set.Add(OpenLab.Crypto.HashUtility.GenerateToken()))
                {
                    break;
                }
            }

            set.Count.ShouldBeEquivalentTo<int>(iterations, "HashUtility should generate the specified number of random salt tokens without duplicates.");
        }
    }
}
