﻿using System;
using System.Collections.Generic;

using FluentAssertions;
using NUnit.Framework;

namespace OpenLab.CryptoTests
{
    [TestFixture]
    public class HashUtility_Password_Tests
    {
        [Test]
        public void HashPassword_Should_Always_Return_NonEmpty_String()
        {
            string password = string.Empty;

            string hash = OpenLab.Crypto.HashUtility.HashPassword(password);

            hash.Should().NotBeNullOrWhiteSpace("Password is empty but not null, and therefore HashUtility should generate a unique password hash.");
        }


        [Test]
        public void HashPassword_Should_Always_Return_Unique_String_For_Same_Password_EmptyPassword()
        {
            string password = string.Empty;

            int iterations = 1000;

            HashSet<string> set = new HashSet<string>();

            for (int i = 0; i < iterations; i++)
            {
                if (!set.Add(OpenLab.Crypto.HashUtility.HashPassword(password)))
                {
                    break;
                }
            }

            set.Count.ShouldBeEquivalentTo<int>(iterations, "HashUtility must generate the specified number of unique hash values from an empty password without duplicates.");
        }


        [Test]
        public void HashPassword_Should_Always_Return_Unique_String_For_Same_Password_NonEmptyPassword()
        {
            string password = "See you later, Space Cowboy...";

            int iterations = 1000;

            HashSet<string> set = new HashSet<string>();

            for (int i = 0; i < iterations; i++)
            {
                if (!set.Add(OpenLab.Crypto.HashUtility.HashPassword(password)))
                {
                    break;
                }
            }

            set.Count.ShouldBeEquivalentTo<int>(iterations, "HashUtility must generate the specified number of unique hash values from a single password without duplicates.");
        }


        [Test]
        public void VerifyHashedPassword_From_HashPassword_NonEmptyPassword()
        {
            string password = "See you later, Space Cowboy...", hashedPassword = OpenLab.Crypto.HashUtility.HashPassword(password);
            bool matches = OpenLab.Crypto.HashUtility.VerifyHashedPassword(hashedPassword, password);

            matches.Should().BeTrue("HashUtility must be able to verify a plaintext password which it has previously hashed.");
        }
    }
}
