﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//
using MahApps.Metro.Controls;
//
using OpenLab.Common;
//
using Appointments.ApplicationServices;

namespace Appointments.Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        IAppointmentService Service { get; set; }

        public MainWindow(IAppointmentService service)
        {
            InitializeComponent();

            Service = service;
        }

        private void Exit()
        {
            Application.Current.Shutdown();
        }
    }
}
