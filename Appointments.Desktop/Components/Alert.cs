﻿namespace Appointments.Desktop
{
    public class Alert
    {
        public const string Debug = "DEBUG";
        public const string Information = "INFO";
        public const string Success = "SUCCESS";
        public const string Warning = "WARNING";
        public const string Error = "ERROR";

        public static string[] ALL
        {
            get
            {
                return new[] { Debug, Information, Success, Warning, Error };
            }
        }
    }
}
