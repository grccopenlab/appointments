﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Appointments.Desktop.Components
{
    public class AssemblyHelper
    {
        public static string GetAssemblyInfo(string key)
        {
            string info = string.Empty;

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            switch (key)
            {
                case "CompanyName":
                    info = fvi.CompanyName;
                    break;

                case "ProductName":
                    info = fvi.ProductName;
                    break;

                case "ProductVersion":
                    info = fvi.ProductVersion;
                    break;
            }

            return info;
        }
    }
}
