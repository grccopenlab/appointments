﻿using System;
using System.Net;
using System.Net.Mail;

namespace Appointments.Desktop
{
    /// <summary>
    /// Utility class. Uses a System.Net.Mail.MailMessage object and a System.Net.Mail.SmtpClient object to
    /// send an email from a specified SMTP server
    /// </summary>
    public partial class GmailHelper
    {
        #region Properties

        MailMessage message;
        SmtpClient client;

        /// <summary>
        /// User name used to authenticate to the remote SMTP server
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password used to authenticate to the remote SMTP server
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The IP address or domain name of the SMTP server
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// The sender of the MailMessage object
        /// </summary>
        public string From
        {
            get { return message.From.ToString(); }
            set
            {
                if ( !value.Equals( string.Empty ) )
                {
                    this.message.From = new MailAddress( value );
                }
            }
        }

        /// <summary>
        /// The subject of the MailMessage object
        /// </summary>
        public string Subject
        {
            get { return message.Subject; }
            set
            {
                if ( !value.Equals( string.Empty ) )
                {
                    this.message.Subject = value;
                }
            }
        }

        /// <summary>
        /// The body of the MailMessage object
        /// </summary>
        public string Body
        {
            get { return message.Body; }
            set
            {
                if ( !value.Equals( string.Empty ) )
                {
                    this.message.Body = value;
                }
            }
        }

        /// <summary>
        /// Application port for the SMTP service
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Credentials used to authenticate to the Mail server (read only)
        /// </summary>
        public NetworkCredential Credentials
        {
            get { return new NetworkCredential( UserName, Password ); }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public GmailHelper()
        {
            message = new MailMessage();
            client = new SmtpClient();
        }

        /// <summary>
        /// Overloaded constructor. Accepts the sender and first (primary) recipient address
        /// </summary>
        /// <param name="from">Sender mail address</param>
        /// <param name="to">Primary recipient mail address</param>
        public GmailHelper( string from, string to )
            : this( from, to, "smtp.gmail.com", 587 )
        {
        }

        /// <summary>
        /// Overloaded constructor. Accepts the sender and first (primary) recipient address, the IP or domain address of 
        /// the SMTP server, and the application port for the SMTP server.
        /// </summary>
        /// <param name="from">Sender mail address</param>
        /// <param name="to">Primary recipient mail address</param>
        /// <param name="host">The IP address or domain name of the SMTP server</param>
        /// <param name="port">Application port for the SMTP service</param>
        public GmailHelper( string from, string to, string host, int port )
        {
            message = new MailMessage( from, to );
            message.IsBodyHtml = true;
            client = new SmtpClient( host, port );
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add a recipient to the To: collection
        /// </summary>
        /// <param name="to">Mail address of the recipient</param>
        /// <returns>true is insertion succeeds; false otherwise</returns>
        public bool AddRecipient( string to )
        {
            try
            {
                message.To.Add( new MailAddress( to ) );
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Add a recipient to the CC: collection
        /// </summary>
        /// <param name="cc">Mail address of the recipient</param>
        /// <returns>true is insertion succeeds; false otherwise</returns>
        public bool AddCarbonCopyRecipient( string cc )
        {
            message.CC.Add( new MailAddress( cc ) );
            return true;
        }

        /// <summary>
        /// Add a recipient to the Bcc: collection
        /// </summary>
        /// <param name="bcc">Mail address of the recipient</param>
        /// <returns>true is insertion succeeds; false otherwise</returns>
        public bool AddBlindCarbonCopyRecipient( string bcc )
        {
            message.Bcc.Add( new MailAddress( bcc ) );
            return true;
        }

        /// <summary>
        /// Send the MailMessage object using the SmtpClient object
        /// </summary>
        /// <remarks>Uses an SSL connection for remote authentication to the SMTP server</remarks>
        public void Send()
        {
            client.Credentials = Credentials;
            client.EnableSsl = true;

            client.Send( message );
        }

        #endregion
    }
}
