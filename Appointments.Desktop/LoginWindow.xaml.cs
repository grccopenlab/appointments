﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//
using MahApps.Metro.Controls;
//
using Appointments.Membership;

namespace Appointments.Desktop
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        internal IMembershipService Membership { get; private set; }

        public LoginWindow(IMembershipService membership)
        {
            InitializeComponent();

            Membership = membership;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string userName = UserNameTextBox.Text.Trim(),
                    password = PasswordTextBox.Password;

                var result = Membership.AuthenticateUser(userName, password);
                if (result == AuthenticationResult.Success)
                {
                    var window = Program.ContainerInstance.GetInstance<MainWindow>();
                    window.Show();

                    this.Close();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
