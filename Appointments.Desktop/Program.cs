﻿using System;
//
using Appointments.ApplicationServices;
using Appointments.Membership;
//
using SimpleInjector;

namespace Appointments.Desktop
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            ContainerInstance = Bootstrap();

            RunApplication(ContainerInstance);
        }

        static Container containerInstance;
        public static Container ContainerInstance
        {
            get
            {
                return containerInstance;
            }
            private set
            {
                containerInstance = value;
            }
        }

        private static Container Bootstrap()
        {
            var container = new Container();

            container.Register<IAppointmentService, AppointmentService>();
            container.Register<IMembershipService, MembershipService>();

            // Register your windows and view models:
            container.Register<LoginWindow>();
            container.Register<MainWindow>();

            container.Verify();

            return container;
        }

        private static void RunApplication(Container container)
        {
            try
            {
                var app = new App();
                var window = container.GetInstance<LoginWindow>();

                // InitializeComponent() must be called, or global resources (those defined in App.xaml) will not be loaded
                app.InitializeComponent();
                app.Run(window);
            }
            catch (Exception ex)
            {
                //Log the exception and exit
            }
        }
    }
}
