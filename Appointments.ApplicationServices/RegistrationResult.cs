﻿namespace Appointments.ApplicationServices
{
    public enum RegistrationResult
    {
        Unknown = 1,
        Success = 2,
        FailedCouldNotSaveChanges = 3,
        FailedDuplicateUserName = 4,
        FailedDuplicateEmailAddress = 5,
        FailedDuplicateEvent = 6,
        FailedTutorNotAvailable = 7,
        FailedLimitExceeded = 8
    }
}