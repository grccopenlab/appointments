﻿using System;
using System.Collections.Generic;
//
using Appointments.Entities;

namespace Appointments.ApplicationServices
{
    public interface IAppointmentService : IDisposable
    {
        bool IsAdminUser(string adminUserName);
        RegistrationResult RegisterUser(string adminUserName, string userName, string password, string displayName, params long[] roles);
        bool RemoveUser(string adminUserName, long userID);
        bool UpdateUser(string adminUserName, long userID, string userName, string displayName, params long[] roles);
        bool SetUserActiveState(string adminUserName, long userID, bool isActive);
        IUserEntity GetUser(string adminUserName, long userID);
        IEnumerable<IUserEntity> GetUsers(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false);

        RegistrationResult RegisterStudent(string adminUserName, string campusID, string firstName, string lastName, string emailAddress, string phoneNumber, bool activateIfExists = false);
        bool UpdateStudent(string adminUserName, long studentID, string campusID, string firstName, string lastName, string emailAddress, string phoneNumber, bool isActive);
        bool SetStudentActiveState(string adminUserName, long studentID, bool isActive);
        bool RemoveStudent(string adminUserName, long studentID);
        IStudentEntity GetStudent(string adminUserName, long studentID);
        IEnumerable<IStudentEntity> GetStudents(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false);

        RegistrationResult RegisterInstructor(string adminUserName, string firstName, string lastName, string emailAddress, bool activateIfExists = false);
        bool UpdateInstructor(string adminUserName, long instructorID, string firstName, string lastName, string emailAddress, bool isActive);
        bool SetInstructorActiveState(string adminUserName, long instructorID, bool isActive);
        bool RemoveInstructor(string adminUserName, long instructorID);
        IInstructorEntity GetInstructor(string adminUserName, long instructorID);
        IEnumerable<IInstructorEntity> GetInstructors(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false);

        RegistrationResult RegisterTutor(string adminUserName, string firstName, string lastName, string emailAddress, string phoneNumber, bool activateIfExists = false, IAvailabilityEntity[] availability = null, ISkillsEntity[] skills = null);
        bool UpdateTutor(string adminUserName, long tutorID, string firstName, string lastName, string emailAddress, string phoneNumber, bool isActive, IAvailabilityEntity[] availability = null, ISkillsEntity[] skills = null);
        bool SetTutorActiveState(string adminUserName, long tutorID, bool isActive);
        bool RemoveTutor(string adminUserName, long tutorID);
        ITutorEntity GetTutor(string adminUserName, long tutorID);
        IEnumerable<ITutorEntity> GetTutors(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false);

        bool AddSubject(string adminUserName, string code, string name);
        bool UpdateSubject(string adminUserName, long subjectID, string code, string name, bool isActive);
        bool RemoveSubject(string adminUserName, long subjectID);

        IEnumerable<ISimpleEntity> GetAlerts();
        IEnumerable<ISimpleEntity> GetLevels();
        IEnumerable<ISimpleEntity> GetRoles();
        IEnumerable<ISimpleEntity> GetSemesters();

        bool AddOrUpdateAlert(string adminUserName, long? alertID, string content, DateTime? showFromDate = null, DateTime? showUntilDate = null);
        bool RemoveAlert(string adminUserName, long alertID);

        RegistrationResult RegisterAppointment(string adminUserName, IAppointmentEntity appointment);
        UpdateResult ModifyAppointment(string adminUserName, IAppointmentEntity appointment);
        UpdateResult CancelAppointment(string adminUserName, long appointmentID, string reason);
        UpdateResult MarkMissedAppointment(string adminUserName, long appointmentID);
        UpdateResult CompleteAppointment(string adminUserName, IAppointmentEntity appointment);
        IAppointmentEntity GetAppointment(long appointmentID);
        IEnumerable<IAppointmentEntity> GetAppointments(int page, int limit, bool descending = false, PagingFilter filter = PagingFilter.None, string filterValue = null);
    }
}
