﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appointments.ApplicationServices
{
    internal partial class EntityMapper
    {
        public static Entities.IUserEntity MapUserToEntity(Data.User user)
        {
            return user == null ? null : new Entities.UserEntity
            {
                ID = user.ID,
                DisplayName = user.DisplayName,
                IsActive = user.IsActive,
                IsLocked = user.IsLocked,
                LockExpires = user.LockExpires,
                LoginAttempts = user.LoginAttempts,
                UserName = user.UserName,
                Roles = user.Roles.Select(role => MapRoleToEntity(role))
            };
        }

        public static Entities.ISimpleEntity MapRoleToEntity(Data.Role role)
        {
            return role == null ? null : new Entities.SimpleEntity
            {
                ID = role.ID,
                Name = role.Name
            };
        }

        public static Entities.ISimpleEntity MapSemesterToEntity(Data.Semester semester)
        {
            return semester == null ? null : new Entities.SimpleEntity
            {
                ID = semester.ID,
                Name = semester.Name
            };
        }

        public static Entities.ISimpleEntity MapSkillLevelToEntity(Data.SkillLevel level)
        {
            return level == null ? null : new Entities.SimpleEntity
            {
                ID = level.ID,
                Name = level.Name
            };
        }

        public static Entities.IInstructorEntity MapInstructorToEntity(Data.Instructor instructor)
        {
            return instructor == null ? null : new Entities.InstructorEntity
            {
                ID = instructor.ID,
                FirstName = instructor.FirstName,
                LastName = instructor.LastName,
                EmailAddress = instructor.EmailAddress,
                IsActive = instructor.IsActive
            };
        }

        public static Entities.IStudentEntity MapStudentToEntity(Data.Student student)
        {
            return student == null ? null : new Entities.StudentEntity
            {
                ID = student.ID,
                CampusID = student.CampusID,
                FirstName = student.FirstName,
                LastName = student.LastName,
                EmailAddress = student.EmailAddress,
                Phone = student.Phone,
                IsActive = student.IsActive
            };
        }

        public static Entities.ITutorEntity MapTutorToEntity(Data.Tutor tutor)
        {
            return tutor == null ? null : new Entities.TutorEntity
            {
                ID = tutor.ID,
                FirstName = tutor.FirstName,
                LastName = tutor.LastName,
                EmailAddress = tutor.EmailAddress,
                Phone = tutor.Phone,
                IsActive = tutor.IsActive
            };
        }

        public static Entities.ISubjectEntity MapSubjectToEntity(Data.Subject subject)
        {
            return subject == null ? null : new Entities.SubjectEntity
            {
                ID = subject.ID,
                Code = subject.Code,
                IsActive = subject.IsActive,
                Name = subject.Name
            };
        }

        public static Entities.IAppointmentEntity MapAppointmentToEntity(Data.Appointment appointment)
        {
            return appointment == null ? null : new Entities.AppointmentEntity
            {
                ID = appointment.ID,
                StudentID = appointment.StudentID,
                Student = MapStudentToEntity(appointment.Student),
                TutorID = appointment.TutorID,
                Tutor = MapTutorToEntity(appointment.Tutor),
                SubjectID = appointment.SubjectID,
                Subject = MapSubjectToEntity(appointment.Subject),
                ScheduledDate = appointment.ScheduledDate,
                NeedsHelpOther = appointment.NeedsHelpOther,
                AreaOrConcept = appointment.AreaOrConcept,
                NeedsHelpWithCoreConcepts = appointment.NeedsHelpWithCoreConcepts,
                NeedsHelpWithCourseSoftware = appointment.NeedsHelpWithCourseSoftware,
                NeedsHelpWithTerminology = appointment.NeedsHelpWithTerminology,
                NeedsHelpWithTextbook = appointment.NeedsHelpWithTextbook,
                ReasonForAppointment = appointment.ReasonForAppointment,
                TextPageReferences = appointment.TextPageReferences,
                Feedback = appointment.Feedback,
                WasTutorHelpful = appointment.WasTutorHelpful,
                WasProblemSolved = appointment.WasProblemSolved,
                WereGoalsMet = appointment.WereGoalsMet,
                IsComplete = appointment.IsComplete,
                WasMissed = appointment.WasMissed,
                CancelledByUserID = appointment.CancelledByUserID,
                CancelledByUser = MapUserToEntity(appointment.CancelledByUser),
                CancelReason = appointment.CancelReason,
                IsCancelled = appointment.IsCancelled
            };
        }
    }
}
