﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using Appointments.Data;
using Appointments.Entities;
//
using OpenLab.Common;
using OpenLab.Crypto;

namespace Appointments.ApplicationServices
{
    public class AppointmentService : IAppointmentService
    {
        Data.OpenLabContext _context;

        Data.OpenLabContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new Data.OpenLabContext();
                }

                return _context;
            }
        }


        #region IDisposable Members

        bool disposed = false;

        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeAll)
        {
            if (!disposed)
            {
                if (disposeAll)
                {
                    _context.Dispose();
                    _context = null;
                }

                disposed = true;
            }
        }

        #endregion


        bool CanManageNonUserEntities(string adminUserName)
        {
            bool canManage = false;

            if (!IsAdminUser(adminUserName))
            {
                var admin = Context.Users.SingleOrDefault(u => u.IsActive && !u.IsLocked && u.UserName.Equals(adminUserName, StringComparison.OrdinalIgnoreCase));
                if (admin != null)
                {
                    canManage = admin.Roles.Any(r => r.Name.Equals(OpenLab.Common.Role.LabManager, StringComparison.OrdinalIgnoreCase));
                }
            }
            else
            {
                canManage = true;
            }

            return canManage;
        }


        public bool IsAdminUser(string adminUserName)
        {
            bool isAdmin = false;

            var admin = Context.Users.SingleOrDefault(u => u.IsActive && !u.IsLocked && u.UserName.Equals(adminUserName, StringComparison.OrdinalIgnoreCase));
            if (admin != null)
            {
                string[] adminRoles = { OpenLab.Common.Role.Coordinator, OpenLab.Common.Role.SystemAdmin };
                isAdmin = admin.Roles.Any(r => adminRoles.Contains(r.Name));
            }

            return isAdmin;
        }

        public bool CanManageStudents(string adminUserName)
        {
            return CanManageNonUserEntities(adminUserName);
        }

        public bool CanManageTutors(string adminUserName)
        {
            return CanManageNonUserEntities(adminUserName);
        }

        public bool CanManageInstructors(string adminUserName)
        {
            return CanManageNonUserEntities(adminUserName);
        }

        public bool CanManageAppointments(string adminUserName)
        {
            return CanManageNonUserEntities(adminUserName);
        }


        public RegistrationResult RegisterUser(string adminUserName, string userName, string password, string displayName, params long[] roles)
        {
            RegistrationResult result = RegistrationResult.Unknown;

            if (IsAdminUser(adminUserName))
            {
                if (Context.Users.Any(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)))
                {
                    result = RegistrationResult.FailedDuplicateUserName;
                }
                else
                {
                    var user = new Data.User
                    {
                        UserName = userName,
                        DisplayName = displayName
                    };

                    string salt = OpenLab.Crypto.HashUtility.GenerateSalt();
                    user.Salt = salt;
                    user.Password = HashUtility.HashPassword(salt + password);
                    foreach (var roleID in roles)
                    {
                        var role = Context.Roles.SingleOrDefault(r => r.ID == roleID);
                        if (role != null)
                        {
                            user.Roles.Add(role);
                        }
                    }

                    Context.Users.Add(user);

                    if (Context.SaveChanges(true) > 0)
                    {
                        result = RegistrationResult.Success;
                    }
                    else
                    {
                        result = RegistrationResult.FailedCouldNotSaveChanges;
                    }
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RegisterUser");
            }

            return result;
        }

        public bool RemoveUser(string adminUserName, long userID)
        {
            bool userRemoved = false;

            if (IsAdminUser(adminUserName))
            {
                var user = Context.Users.Include(u => u.Roles).SingleOrDefault(u => u.ID == userID);
                if (user != null)
                {
                    if (user.CancelledAppointments == null || user.CancelledAppointments.Count == 0)
                    {
                        Context.Users.Remove(user);
                        userRemoved = Context.SaveChanges(true) > 0;
                    }
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RemoveUser");
            }

            return userRemoved;
        }

        public bool UpdateUser(string adminUserName, long userID, string userName, string displayName, params long[] roles)
        {
            bool userUpdated = false;

            if (IsAdminUser(adminUserName))
            {
                var user = Context.Users.Include(u => u.Roles).SingleOrDefault(u => u.ID == userID);
                if (user != null)
                {
                    if (!user.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase))
                    {
                        if (Context.Users.Any(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)))
                        {
                            throw new DuplicateUserNameException(userName);
                        }
                    }
                    else
                    {
                        user.UserName = userName;
                    }

                    var currentRoles = user.Roles.ToList();
                    foreach (var role in currentRoles)
                    {
                        if (!roles.Contains(role.ID))
                        {
                            user.Roles.Remove(role);
                        }
                    }

                    foreach (var roleID in roles)
                    {
                        var role = user.Roles.FirstOrDefault(r => r.ID == roleID);
                        if (role == null)
                        {
                            user.Roles.Add(role);
                        }
                    }

                    user.DisplayName = displayName;

                    userUpdated = Context.SaveChanges(true) > 0;
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.UpdateUser");
            }

            return userUpdated;
        }

        public bool SetUserActiveState(string adminUserName, long userID, bool isActive)
        {
            bool userUpdated = false;

            if (IsAdminUser(adminUserName))
            {
                var user = Context.Users.SingleOrDefault(u => u.ID == userID);
                if (user != null)
                {
                    user.IsActive = isActive;

                    userUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.SetUserActiveState");
            }

            return userUpdated;
        }

        public IUserEntity GetUser(string adminUserName, long userID)
        {
            if (IsAdminUser(adminUserName))
            {
                return EntityMapper.MapUserToEntity(Context.Users.SingleOrDefault(u => u.ID == userID));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetUser");
            }
        }

        public IEnumerable<IUserEntity> GetUsers(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false)
        {
            if (IsAdminUser(adminUserName))
            {
                return Context.Users
                    .SortedBy(sortBy, descending)
                    .Skip((page - 1) * perPage)
                    .Take(perPage)
                    .AsEnumerable()
                    .Select(user => EntityMapper.MapUserToEntity(user));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetUsers");
            }
        }


        public RegistrationResult RegisterStudent(string adminUserName, string campusID, string firstName, string lastName, string emailAddress, string phoneNumber, bool activateIfExists = false)
        {
            RegistrationResult result = RegistrationResult.Unknown;

            if (CanManageStudents(adminUserName))
            {
                if (Context.Students.Any(s => s.IsActive && s.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                {
                    result = RegistrationResult.FailedDuplicateEmailAddress;
                }
                else
                {
                    Data.Student student = null;

                    if (Context.Students.Any(s => !s.IsActive && s.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                    {
                        if (activateIfExists)
                        {
                            student = Context.Students.SingleOrDefault(s => s.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase));
                            student.IsActive = true;
                        }
                        else
                        {
                            result = RegistrationResult.FailedDuplicateEmailAddress;
                        }
                    }
                    else
                    {
                        student = new Data.Student
                        {
                            IsActive = true
                        };

                        Context.Students.Add(student);
                    }

                    if (student != null)
                    {
                        student.CampusID = campusID;
                        student.FirstName = firstName;
                        student.LastName = lastName;
                        student.EmailAddress = emailAddress;

                        if (!string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            student.Phone = phoneNumber;
                        }

                        if (Context.SaveChanges(true) > 0)
                        {
                            result = RegistrationResult.Success;
                        }
                        else
                        {
                            result = RegistrationResult.FailedCouldNotSaveChanges;
                        }
                    }
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RegisterStudent");
            }

            return result;
        }

        public bool UpdateStudent(string adminUserName, long studentID, string campusID, string firstName, string lastName, string emailAddress, string phoneNumber, bool isActive)
        {
            bool studentUpdated = false;

            if (CanManageStudents(adminUserName))
            {
                var student = Context.Students.SingleOrDefault(s => s.ID == studentID);
                if (student != null)
                {
                    if (!student.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase))
                    {
                        if (Context.Students.Any(s => s.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                        {
                            throw new DuplicateEmailAddressException(emailAddress);
                        }
                        else
                        {
                            student.EmailAddress = emailAddress;
                        }
                    }

                    student.CampusID = campusID;
                    student.FirstName = firstName;
                    student.LastName = lastName;

                    if (!string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        student.Phone = phoneNumber;
                    }
                    else
                    {
                        student.Phone = null;
                    }

                    studentUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.UpdateStudent");
            }

            return studentUpdated;
        }

        public bool SetStudentActiveState(string adminUserName, long studentID, bool isActive)
        {
            bool studentUpdated = false;

            if (CanManageStudents(adminUserName))
            {
                var student = Context.Students.SingleOrDefault(s => s.ID == studentID);
                if (student != null)
                {
                    student.IsActive = isActive;

                    studentUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.SetStudentActiveState");
            }

            return studentUpdated;
        }

        public bool RemoveStudent(string adminUserName, long studentID)
        {
            if (CanManageStudents(adminUserName))
            {
                bool studentRemoved = false;
                var student = Context.Students
                    .Include(s => s.Appointments)
                    .Include(s => s.StudentSubjects)
                    .SingleOrDefault(s => s.ID == studentID);

                if (student != null)
                {
                    if (student.Appointments.Any())
                    {
                        var appointments = student.Appointments.ToList();
                        Context.Appointments.RemoveRange(appointments);
                    }

                    if (student.StudentSubjects.Any())
                    {
                        var subjects = student.StudentSubjects.ToList();
                        Context.StudentSubjects.RemoveRange(subjects);
                    }

                    Context.Students.Remove(student);

                    studentRemoved = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                }

                return studentRemoved;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RemoveStudent");
            }
        }

        public IStudentEntity GetStudent(string adminUserName, long studentID)
        {
            if (CanManageStudents(adminUserName))
            {
                return EntityMapper.MapStudentToEntity(Context.Students.SingleOrDefault(student => student.ID == studentID));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetStudent");
            }
        }

        public IEnumerable<IStudentEntity> GetStudents(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false)
        {
            if (CanManageStudents(adminUserName))
            {
                return Context.Students
                    .SortedBy(sortBy, descending)
                    .Skip((page - 1) * perPage)
                    .Take(perPage)
                    .AsEnumerable()
                    .Select(student => EntityMapper.MapStudentToEntity(student));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetStudents");
            }
        }


        public RegistrationResult RegisterInstructor(string adminUserName, string firstName, string lastName, string emailAddress, bool activateIfExists = false)
        {
            RegistrationResult result = RegistrationResult.Unknown;

            if (CanManageInstructors(adminUserName))
            {
                if (Context.Instructors.Any(i => i.IsActive && i.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                {
                    result = RegistrationResult.FailedDuplicateEmailAddress;
                }
                else
                {
                    Data.Instructor instructor = null;

                    if (Context.Instructors.Any(i => !i.IsActive && i.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                    {
                        if (activateIfExists)
                        {
                            instructor = Context.Instructors.SingleOrDefault(i => i.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase));
                            instructor.IsActive = true;
                        }
                        else
                        {
                            result = RegistrationResult.FailedDuplicateEmailAddress;
                        }
                    }
                    else
                    {
                        instructor = new Data.Instructor
                        {
                            IsActive = true
                        };

                        Context.Instructors.Add(instructor);
                    }

                    if (instructor != null)
                    {
                        instructor.FirstName = firstName;
                        instructor.LastName = lastName;
                        instructor.EmailAddress = emailAddress;

                        if (Context.SaveChanges(true) > 0)
                        {
                            result = RegistrationResult.Success;
                        }
                        else
                        {
                            result = RegistrationResult.FailedCouldNotSaveChanges;
                        }
                    }
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RegisterInstructor");
            }

            return result;
        }

        public bool UpdateInstructor(string adminUserName, long instructorID, string firstName, string lastName, string emailAddress, bool isActive)
        {
            bool instructorUpdated = false;

            if (CanManageInstructors(adminUserName))
            {
                var instructor = Context.Instructors.SingleOrDefault(i => i.ID == instructorID);
                if (instructor != null)
                {
                    if (!instructor.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase))
                    {
                        if (Context.Instructors.Any(i => i.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                        {
                            throw new DuplicateEmailAddressException(emailAddress);
                        }
                        else
                        {
                            instructor.EmailAddress = emailAddress;
                        }
                    }

                    instructor.FirstName = firstName;
                    instructor.LastName = lastName;

                    instructorUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.UpdateInstructor");
            }

            return instructorUpdated;
        }

        public bool SetInstructorActiveState(string adminUserName, long instructorID, bool isActive)
        {
            bool instructorUpdated = false;

            if (CanManageInstructors(adminUserName))
            {
                var instructor = Context.Instructors.SingleOrDefault(i => i.ID == instructorID);
                if (instructor != null)
                {
                    instructor.IsActive = isActive;

                    instructorUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.SetInstructorActiveState");
            }

            return instructorUpdated;
        }

        public bool RemoveInstructor(string adminUserName, long instructorID)
        {
            if (CanManageInstructors(adminUserName))
            {
                bool instructorRemoved = false;
                var instructor = Context.Instructors
                    .Include(i => i.StudentSubjects)
                    .SingleOrDefault(i => i.ID == instructorID);

                if (instructor != null)
                {
                    if (!instructor.StudentSubjects.Any())
                    {
                        Context.Instructors.Remove(instructor);

                        instructorRemoved = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                    }
                }

                return instructorRemoved;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RemoveInstructor");
            }
        }

        public IInstructorEntity GetInstructor(string adminUserName, long instructorID)
        {
            if (CanManageInstructors(adminUserName))
            {
                return EntityMapper.MapInstructorToEntity(Context.Instructors.SingleOrDefault(instructor => instructor.ID == instructorID));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetInstructor");
            }
        }

        public IEnumerable<IInstructorEntity> GetInstructors(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false)
        {
            if (CanManageInstructors(adminUserName))
            {
                return Context.Instructors
                    .SortedBy(sortBy, descending)
                    .Skip((page - 1) * perPage)
                    .Take(perPage)
                    .AsEnumerable()
                    .Select(instructor => EntityMapper.MapInstructorToEntity(instructor));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetInstructors");
            }
        }


        public RegistrationResult RegisterTutor(string adminUserName, string firstName, string lastName, string emailAddress, string phoneNumber, bool activateIfExists = false, IAvailabilityEntity[] availability = null, ISkillsEntity[] skills = null)
        {
            RegistrationResult result = RegistrationResult.Unknown;

            if (CanManageTutors(adminUserName))
            {
                if (Context.Tutors.Any(t => t.IsActive && t.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                {
                    result = RegistrationResult.FailedDuplicateEmailAddress;
                }
                else
                {
                    Data.Tutor tutor = null;

                    if (Context.Tutors.Any(t => !t.IsActive && t.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                    {
                        if (activateIfExists)
                        {
                            tutor = Context.Tutors.SingleOrDefault(i => i.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase));
                            tutor.IsActive = true;
                        }
                        else
                        {
                            result = RegistrationResult.FailedDuplicateEmailAddress;
                        }
                    }
                    else
                    {
                        tutor = new Data.Tutor
                        {
                            IsActive = true
                        };

                        Context.Tutors.Add(tutor);
                    }

                    if (tutor != null)
                    {
                        tutor.FirstName = firstName;
                        tutor.LastName = lastName;
                        tutor.EmailAddress = emailAddress;

                        if (!string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            tutor.Phone = phoneNumber;
                        }

                        foreach (var day in availability)
                        {
                            Context.TutorAvailabilities.Add(new TutorAvailability
                            {
                                DayOfWeek = day.DayOfWeek,
                                EndTime = day.EndTime,
                                StartTime = day.StartTime,
                                SemesterID = day.SemesterID,
                                Tutor = tutor,
                                IsActive = true
                            });
                        }

                        foreach (var skill in skills)
                        {
                            Context.TutorSubjects.Add(new TutorSubject
                            {
                                SkillLevelID = skill.LevelID,
                                SubjectID = skill.SubjectID,
                                Tutor = tutor,
                                IsActive = true
                            });
                        }

                        if (Context.SaveChanges(true) > 0)
                        {
                            result = RegistrationResult.Success;
                        }
                        else
                        {
                            result = RegistrationResult.FailedCouldNotSaveChanges;
                        }
                    }
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RegisterTutor");
            }

            return result;
        }

        public bool UpdateTutor(string adminUserName, long tutorID, string firstName, string lastName, string emailAddress, string phoneNumber, bool isActive, IAvailabilityEntity[] availability = null, ISkillsEntity[] skills = null)
        {
            bool tutorUpdated = false;

            if (CanManageTutors(adminUserName))
            {
                var tutor = Context.Tutors
                    .Include(t => t.TutorAvailabilities)
                    .Include(t => t.TutorSubjects)
                    .SingleOrDefault(t => t.ID == tutorID);

                if (tutor != null)
                {
                    if (!tutor.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase))
                    {
                        if (Context.Tutors.Any(t => t.EmailAddress.Equals(emailAddress, StringComparison.OrdinalIgnoreCase)))
                        {
                            throw new DuplicateEmailAddressException(emailAddress);
                        }
                        else
                        {
                            tutor.EmailAddress = emailAddress;
                        }
                    }

                    tutor.FirstName = firstName;
                    tutor.LastName = lastName;

                    if (!string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        tutor.Phone = phoneNumber;
                    }
                    else
                    {
                        tutor.Phone = null;
                    }

                    var currentAvailability = tutor.TutorAvailabilities.ToList();
                    Context.TutorAvailabilities.RemoveRange(currentAvailability.Where(ta => !availability.Select(x => x.ID).Contains(ta.ID)));
                    foreach (var day in availability)
                    {
                        if (day.ID == 0)
                        {
                            Context.TutorAvailabilities.Add(new TutorAvailability
                            {
                                DayOfWeek = day.DayOfWeek,
                                EndTime = day.EndTime,
                                StartTime = day.StartTime,
                                SemesterID = day.SemesterID,
                                Tutor = tutor,
                                IsActive = true
                            });
                        }
                        else
                        {
                            var available = tutor.TutorAvailabilities.SingleOrDefault(ta => ta.ID == day.ID);
                            if (available != null)
                            {
                                available.DayOfWeek = day.DayOfWeek;
                                available.EndTime = day.EndTime;
                                available.StartTime = day.StartTime;
                                available.SemesterID = day.SemesterID;
                            }
                        }
                    }

                    var currentSkills = tutor.TutorSubjects.ToList();
                    Context.TutorSubjects.RemoveRange(currentSkills.Where(ts => !skills.Select(x => x.ID).Contains(ts.ID)));
                    foreach (var skill in skills)
                    {
                        if (skill.ID == 0)
                        {
                            Context.TutorSubjects.Add(new TutorSubject
                            {
                                SkillLevelID = skill.LevelID,
                                SubjectID = skill.SubjectID,
                                Tutor = tutor,
                                IsActive = true
                            });
                        }
                        else
                        {
                            var tutorSubject = tutor.TutorSubjects.SingleOrDefault(ts => ts.ID == skill.ID);
                            if (tutorSubject != null)
                            {
                                tutorSubject.SkillLevelID = skill.LevelID;
                                tutorSubject.SubjectID = skill.SubjectID;
                            }
                        }
                    }

                    tutorUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.UpdateTutor");
            }

            return tutorUpdated;
        }

        public bool SetTutorActiveState(string adminUserName, long tutorID, bool isActive)
        {
            bool tutorUpdated = false;

            if (CanManageTutors(adminUserName))
            {
                var tutor = Context.Tutors.SingleOrDefault(s => s.ID == tutorID);
                if (tutor != null)
                {
                    tutor.IsActive = isActive;

                    tutorUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.SetTutorActiveState");
            }

            return tutorUpdated;
        }

        public bool RemoveTutor(string adminUserName, long tutorID)
        {
            if (CanManageTutors(adminUserName))
            {
                bool tutorRemoved = false;
                var tutor = Context.Tutors
                    .Include(t => t.Appointments)
                    .Include(t => t.TutorAvailabilities)
                    .Include(t => t.TutorSubjects)
                    .SingleOrDefault(i => i.ID == tutorID);

                if (tutor != null)
                {
                    if (!tutor.Appointments.Any())
                    {
                        Context.TutorAvailabilities.RemoveRange(tutor.TutorAvailabilities);
                        Context.TutorSubjects.RemoveRange(tutor.TutorSubjects);

                        Context.Tutors.Remove(tutor);

                        tutorRemoved = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                    }
                }

                return tutorRemoved;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RemoveTutor");
            }
        }

        public ITutorEntity GetTutor(string adminUserName, long tutorID)
        {
            if (CanManageTutors(adminUserName))
            {
                return EntityMapper.MapTutorToEntity(Context.Tutors.SingleOrDefault(tutor => tutor.ID == tutorID));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetTutor");
            }
        }

        public IEnumerable<ITutorEntity> GetTutors(string adminUserName, int page = 1, int perPage = 15, string sortBy = "ID", bool descending = false)
        {
            if (CanManageTutors(adminUserName))
            {
                return Context.Tutors
                    .SortedBy(sortBy, descending)
                    .Skip((page - 1) * perPage)
                    .Take(perPage)
                    .AsEnumerable()
                    .Select(tutor => EntityMapper.MapTutorToEntity(tutor));
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.GetTutors");
            }
        }


        public bool AddSubject(string adminUserName, string code, string name)
        {
            if (CanManageNonUserEntities(adminUserName))
            {
                bool subjectAdded = false;

                Context.Subjects.Add(new Data.Subject
                {
                    Code = code,
                    Name = name,
                    IsActive = true
                });

                subjectAdded = Context.SaveChanges(true) > 0;

                return subjectAdded;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.AddSubject");
            }
        }

        public bool UpdateSubject(string adminUserName, long subjectID, string code, string name, bool isActive)
        {
            if (CanManageNonUserEntities(adminUserName))
            {
                bool subjectUpdated = false;

                var subject = Context.Subjects.SingleOrDefault(s => s.ID == subjectID);
                if (subject != null)
                {
                    subject.Code = code;
                    subject.Name = name;
                    subject.IsActive = isActive;

                    subjectUpdated = Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges();
                }

                return subjectUpdated;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.UpdateSubject");
            }
        }

        public bool RemoveSubject(string adminUserName, long subjectID)
        {
            if (CanManageNonUserEntities(adminUserName))
            {
                bool subjectRemoved = false;

                var subject = Context.Subjects
                    .Include(s => s.StudentSubjects)
                    .Include(s => s.TutorSubjects)
                    .SingleOrDefault(s => s.ID == subjectID);

                if (subject != null)
                {
                    if (!subject.StudentSubjects.Any())
                    {
                        Context.TutorSubjects.RemoveRange(subject.TutorSubjects.ToList());
                        Context.Subjects.Remove(subject);

                        subjectRemoved = Context.SaveChanges(true) > 0;
                    }
                }

                return subjectRemoved;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RemoveSubject");
            }
        }


        public IEnumerable<ISimpleEntity> GetLevels()
        {
            return Context.SkillLevels
                .AsEnumerable()
                .Select(level => EntityMapper.MapSkillLevelToEntity(level));
        }

        public IEnumerable<ISimpleEntity> GetRoles()
        {
            return Context.Roles
                .AsEnumerable()
                .Select(role => EntityMapper.MapRoleToEntity(role));
        }

        public IEnumerable<ISimpleEntity> GetSemesters()
        {
            return Context.Semesters
                .AsEnumerable()
                .Select(semester => EntityMapper.MapSemesterToEntity(semester));
        }


        public IEnumerable<ISimpleEntity> GetAlerts()
        {
            throw new NotImplementedException();
        }

        public bool AddOrUpdateAlert(string adminUserName, long? alertID, string content, DateTime? showFromDate = null, DateTime? showUntilDate = null)
        {
            throw new NotImplementedException();
        }

        public bool RemoveAlert(string adminUserName, long alertID)
        {
            throw new NotImplementedException();
        }


        public IAppointmentEntity GetAppointment(long appointmentID)
        {
            return EntityMapper.MapAppointmentToEntity(Context.Appointments.SingleOrDefault(a => a.ID == appointmentID));
        }

        public IEnumerable<IAppointmentEntity> GetAppointments(int page, int limit, bool descending = false, PagingFilter filter = PagingFilter.None, string filterValue = null)
        {
            //TODO: Implement filter logic

            IEnumerable<IAppointmentEntity> appointments = null;

            if (page < 1) { page = 1; }
            if (limit < 1) { limit = 1; }

            if (descending)
            {
                appointments = Context.Appointments
                    .OrderByDescending(a => a.ID)
                    .Skip((page - 1) * limit)
                    .Take(limit)
                    .AsEnumerable()
                    .Select(a => EntityMapper.MapAppointmentToEntity(a));
            }
            else
            {
                appointments = Context.Appointments
                    .OrderBy(a => a.ID)
                    .Skip((page - 1) * limit)
                    .Take(limit)
                    .AsEnumerable()
                    .Select(a => EntityMapper.MapAppointmentToEntity(a));
            }

            return appointments;
        }

        public RegistrationResult RegisterAppointment(string adminUserName, IAppointmentEntity appointment)
        {
            if (CanManageAppointments(adminUserName))
            {
                RegistrationResult result = RegistrationResult.Unknown;

                Context.Appointments.Add(new Data.Appointment
                {
                    StudentID = appointment.StudentID,
                    TutorID = appointment.TutorID,
                    SubjectID = appointment.SubjectID,
                    ScheduledDate = appointment.ScheduledDate,
                    NeedsHelpOther = appointment.NeedsHelpOther,
                    AreaOrConcept = appointment.AreaOrConcept,
                    NeedsHelpWithCoreConcepts = appointment.NeedsHelpWithCoreConcepts,
                    NeedsHelpWithCourseSoftware = appointment.NeedsHelpWithCourseSoftware,
                    NeedsHelpWithTerminology = appointment.NeedsHelpWithTerminology,
                    NeedsHelpWithTextbook = appointment.NeedsHelpWithTextbook,
                    ReasonForAppointment = appointment.ReasonForAppointment,
                    TextPageReferences = appointment.TextPageReferences
                });

                if (Context.SaveChanges(true) > 0)
                {
                    result = RegistrationResult.Success;
                }
                else
                {
                    result = RegistrationResult.FailedCouldNotSaveChanges;
                }

                return result;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RegisterAppointment");
            }
        }

        public UpdateResult ModifyAppointment(string adminUserName, IAppointmentEntity appointment)
        {
            if (CanManageAppointments(adminUserName))
            {
                UpdateResult result = UpdateResult.Unknown;

                var record = Context.Appointments.SingleOrDefault(a => a.ID == appointment.ID);
                if (record != null)
                {
                    record.StudentID = appointment.StudentID;
                    record.TutorID = appointment.TutorID;
                    record.SubjectID = appointment.SubjectID;
                    record.ScheduledDate = appointment.ScheduledDate;
                    record.NeedsHelpOther = appointment.NeedsHelpOther;
                    record.AreaOrConcept = appointment.AreaOrConcept;
                    record.NeedsHelpWithCoreConcepts = appointment.NeedsHelpWithCoreConcepts;
                    record.NeedsHelpWithCourseSoftware = appointment.NeedsHelpWithCourseSoftware;
                    record.NeedsHelpWithTerminology = appointment.NeedsHelpWithTerminology;
                    record.NeedsHelpWithTextbook = appointment.NeedsHelpWithTextbook;
                    record.ReasonForAppointment = appointment.ReasonForAppointment;
                    record.TextPageReferences = appointment.TextPageReferences;
                    record.Feedback = appointment.Feedback;
                    record.WasTutorHelpful = appointment.WasTutorHelpful;
                    record.WasProblemSolved = appointment.WasProblemSolved;
                    record.WereGoalsMet = appointment.WereGoalsMet;
                    record.IsComplete = appointment.IsComplete;
                    record.WasMissed = appointment.WasMissed;

                    if (Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges())
                    {
                        result = UpdateResult.Success;
                    }
                    else
                    {
                        result = UpdateResult.FailedCouldNotSaveChanges;
                    }
                }
                else
                {
                    result = UpdateResult.FailedRecordNotFound;
                }

                return result;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.RescheduleAppointment");
            }
        }

        public UpdateResult CancelAppointment(string adminUserName, long appointmentID, string reason)
        {
            if (CanManageAppointments(adminUserName))
            {
                UpdateResult result = UpdateResult.Unknown;

                var admin = Context.Users.SingleOrDefault(u => u.IsActive && !u.IsLocked && u.UserName.Equals(adminUserName, StringComparison.OrdinalIgnoreCase));
                if (admin != null)
                {
                    var appointment = Context.Appointments.SingleOrDefault(a => a.ID == appointmentID);
                    if (appointment != null)
                    {
                        appointment.CancelledByUserID = admin.ID;
                        appointment.CancelReason = reason;
                        appointment.IsCancelled = true;

                        if (Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges())
                        {
                            result = UpdateResult.Success;
                        }
                        else
                        {
                            result = UpdateResult.FailedCouldNotSaveChanges;
                        }
                    }
                    else
                    {
                        result = UpdateResult.FailedRecordNotFound;
                    }
                }

                return result;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.CancelAppointment");
            }
        }

        public UpdateResult MarkMissedAppointment(string adminUserName, long appointmentID)
        {
            if (CanManageAppointments(adminUserName))
            {
                UpdateResult result = UpdateResult.Unknown;

                var appointment = Context.Appointments.SingleOrDefault(a => a.ID == appointmentID);
                if (appointment != null)
                {
                    appointment.WasMissed = true;

                    if (Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges())
                    {
                        result = UpdateResult.Success;
                    }
                    else
                    {
                        result = UpdateResult.FailedCouldNotSaveChanges;
                    }
                }
                else
                {
                    result = UpdateResult.FailedRecordNotFound;
                }

                return result;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.MarkMissedAppointment");
            }
        }

        public UpdateResult CompleteAppointment(string adminUserName, IAppointmentEntity appointment)
        {
            if (CanManageAppointments(adminUserName))
            {
                UpdateResult result = UpdateResult.Unknown;

                var record = Context.Appointments.SingleOrDefault(a => a.ID == appointment.ID);
                if (record != null)
                {
                    record.IsComplete = true;

                    if (Context.SaveChanges(true) > 0 || !Context.ChangeTracker.HasChanges())
                    {
                        result = UpdateResult.Success;
                    }
                    else
                    {
                        result = UpdateResult.FailedCouldNotSaveChanges;
                    }
                }
                else
                {
                    result = UpdateResult.FailedRecordNotFound;
                }

                return result;
            }
            else
            {
                throw new NotAuthorizedForActionException("IAppointmentService.CompleteAppointment");
            }
        }

    }
}
