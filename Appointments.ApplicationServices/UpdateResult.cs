﻿namespace Appointments.ApplicationServices
{
    public enum UpdateResult
    {
        Unknown = 1,
        Success = 2,
        FailedRecordNotFound = 3,
        FailedInvalidUpdateValues = 4,
        FailedCouldNotSaveChanges = 5
    }
}
