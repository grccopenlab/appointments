﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace OpenLab.Crypto
{
    public class HashUtility
    {
        private const int PBKDF2IterCount = 1000; // default for Rfc2898DeriveBytes
        private const int PBKDF2SubkeyLength = 256 / 8; // 256 bits
        private const int SaltSize = 128 / 8; // 128 bits

        public static string GenerateSalt(int size = 64)
        {
            var crypto = new RNGCryptoServiceProvider();
            byte[] rbytes = new byte[size / 2];
            crypto.GetNonZeroBytes(rbytes);

            return ToHexString(rbytes);
        }

        public static string GenerateToken(int size = 64)
        {
            var crypto = new RNGCryptoServiceProvider();
            byte[] rbytes = new byte[size / 2];
            crypto.GetNonZeroBytes(rbytes);

            return ToHexString(rbytes, true);
        }

        private static string ToHexString(byte[] bytes, bool useLowerCase = false)
        {
            var hex = string.Concat(bytes.Select(b => b.ToString(useLowerCase ? "x2" : "X2")));

            return hex;
        }


        /// <summary>
        /// Gets the hash.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="hashMode">Type of hash algorithm used to hash the text.</param>
        /// <param name="forceUpperCase">if set to <c>true</c>, forces uppercase characters in the hash-string.</param>
        /// <param name="unicode">if set to <c>true</c>, UTF-8 encoded bytes are extracted from the source string; otherwise, bytes are ASCII encoded.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Invalid hash type; hashMode</exception>
        /// <remarks>
        /// This method uses UTF-8 encoding by default.
        /// </remarks>
        public static string GetHash(string text, HashMode hashMode, bool forceUpperCase = false, bool unicode = true)
        {
            HashAlgorithm algorithm;
            switch (hashMode)
            {
                case HashMode.MD5:
                    algorithm = MD5Cng.Create();
                    break;
                case HashMode.SHA1:
                    algorithm = SHA1Cng.Create();
                    break;
                case HashMode.SHA256:
                    algorithm = SHA256Cng.Create();
                    break;
                case HashMode.SHA512:
                    algorithm = SHA512Cng.Create();
                    break;
                case HashMode.HMACMD5:
                    algorithm = HMACMD5.Create();
                    break;
                case HashMode.HMACSHA1:
                    algorithm = HMACSHA1.Create();
                    break;
                case HashMode.HMACSHA256:
                    algorithm = HMACSHA256.Create();
                    break;
                case HashMode.HMACSHA512:
                    algorithm = HMACSHA512.Create();
                    break;
                default:
                    throw new ArgumentException("Invalid HashMode", "hashMode");
            }
            byte[] bytes = unicode ? Encoding.UTF8.GetBytes(text) : Encoding.ASCII.GetBytes(text);
            byte[] hash = algorithm.ComputeHash(bytes);

            return ToHexString(hash, !forceUpperCase);
        }

        // Original Version Copyright:
        // Copyright (c) Microsoft Open Technologies, Inc. All rights reserved.
        // Original License: http://www.apache.org/licenses/LICENSE-2.0

        /* =======================
         * HASHED PASSWORD FORMATS
         * =======================
         * 
         * Version 0:
         * PBKDF2 with HMAC-SHA1, 128-bit salt, 256-bit subkey, 1000 iterations.
         * (See also: SDL crypto guidelines v5.1, Part III)
         * Format: { 0x00, salt, subkey }
         */

        public static string HashPassword(string password, int iterationCount = PBKDF2IterCount)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            // Produce a version 0 (see comment above) password hash.
            byte[] salt;
            byte[] subkey;
            using (var deriveBytes = new Rfc2898DeriveBytes(password, SaltSize, iterationCount))
            {
                salt = deriveBytes.Salt;
                subkey = deriveBytes.GetBytes(PBKDF2SubkeyLength);
            }

            byte[] outputBytes = new byte[1 + SaltSize + PBKDF2SubkeyLength];
            Buffer.BlockCopy(salt, 0, outputBytes, 1, SaltSize);
            Buffer.BlockCopy(subkey, 0, outputBytes, 1 + SaltSize, PBKDF2SubkeyLength);
            return Convert.ToBase64String(outputBytes);
        }

        // hashedPassword must be of the format of HashWithPassword (salt + Hash(salt+input)
        public static bool VerifyHashedPassword(string hashedPassword, string password, int iterationCount = PBKDF2IterCount)
        {
            if (hashedPassword == null)
            {
                throw new ArgumentNullException("hashedPassword");
            }
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            byte[] hashedPasswordBytes = Convert.FromBase64String(hashedPassword);

            // Verify a version 0 (see comment above) password hash.

            if (hashedPasswordBytes.Length != (1 + SaltSize + PBKDF2SubkeyLength) || hashedPasswordBytes[0] != 0x00)
            {
                // Wrong length or version header.
                return false;
            }

            byte[] salt = new byte[SaltSize];
            Buffer.BlockCopy(hashedPasswordBytes, 1, salt, 0, SaltSize);
            byte[] storedSubkey = new byte[PBKDF2SubkeyLength];
            Buffer.BlockCopy(hashedPasswordBytes, 1 + SaltSize, storedSubkey, 0, PBKDF2SubkeyLength);

            byte[] generatedSubkey;
            using (var deriveBytes = new Rfc2898DeriveBytes(password, salt, iterationCount))
            {
                generatedSubkey = deriveBytes.GetBytes(PBKDF2SubkeyLength);
            }
            return ByteArraysEqual(storedSubkey, generatedSubkey);
        }

        internal static string BinaryToHex(byte[] data)
        {
            char[] hex = new char[data.Length * 2];

            for (int iter = 0; iter < data.Length; iter++)
            {
                byte hexChar = ((byte)(data[iter] >> 4));
                hex[iter * 2] = (char)(hexChar > 9 ? hexChar + 0x37 : hexChar + 0x30);
                hexChar = ((byte)(data[iter] & 0xF));
                hex[(iter * 2) + 1] = (char)(hexChar > 9 ? hexChar + 0x37 : hexChar + 0x30);
            }
            return new string(hex);
        }

        // Compares two byte arrays for equality. The method is specifically written so that the loop is not optimized.
        [MethodImpl(MethodImplOptions.NoOptimization)]
        private static bool ByteArraysEqual(byte[] a, byte[] b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if (a == null || b == null || a.Length != b.Length)
            {
                return false;
            }

            bool areSame = true;
            for (int i = 0; i < a.Length; i++)
            {
                areSame &= (a[i] == b[i]);
            }
            return areSame;
        }
    }
}
