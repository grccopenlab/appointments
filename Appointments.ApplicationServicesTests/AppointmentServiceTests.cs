﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Appointments.ApplicationServices;

using FluentAssertions;
using NUnit.Framework;

namespace Appointments.ApplicationServices.Tests
{
    [TestFixture()]
    public class AppointmentServiceTests
    {
        [Test()]
        public void IsAdminUserTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void CanManageStudentsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void CanManageTutorsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void CanManageInstructorsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void CanManageAppointmentsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RegisterUserTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RemoveUserTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void UpdateUserTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void SetUserActiveStateTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetUserTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetUsersTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RegisterStudentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void UpdateStudentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void SetStudentActiveStateTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RemoveStudentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetStudentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetStudentsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RegisterInstructorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void UpdateInstructorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void SetInstructorActiveStateTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RemoveInstructorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetInstructorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetInstructorsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RegisterTutorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void UpdateTutorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void SetTutorActiveStateTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RemoveTutorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetTutorTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetTutorsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void AddSubjectTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void UpdateSubjectTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RemoveSubjectTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetLevelsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetRolesTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetSemestersTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void GetAlertsTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void AddOrUpdateAlertTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RemoveAlertTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RegisterAppointmentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void RescheduleAppointmentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void CancelAppointmentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void MarkMissedAppointmentTest()
        {
            Assert.Fail();
        }

        [Test()]
        public void CompleteAppointmentTest()
        {
            Assert.Fail();
        }
    }
}
