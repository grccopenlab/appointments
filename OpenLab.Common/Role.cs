﻿namespace OpenLab.Common
{
    public class Role
    {
        public const string LabManager = "Lab Manager";
        public const string Coordinator = "Coordinator";
        public const string SystemAdmin = "System Admin";
    }
}
