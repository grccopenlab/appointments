﻿using System.Configuration;

namespace OpenLab.Common
{
    public class AppSettings
    {
        public static string ReturnEmail
        {
            get
            {
                return Find("RETURN_EMAIL");
            }
        }

        public static string UserName
        {
            get
            {
                return Find("USER_NAME");
            }
        }

        public static string Password
        {
            get
            {
                return Find("PASSWORD");
            }
        }

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["OpenLab"].ConnectionString;
            }
        }

        public static int PasswordRestWindow
        {
            get
            {
                int window = 120, temp = 0;
                string value = Find("PasswordRestWindow");

                if (int.TryParse(value, out temp))
                {
                    window = temp;
                }

                return window;
            }
        }

        public static int LockOutWindow
        {
            get
            {
                int window = 15, temp = 0;
                string value = Find("LockOutWindow");

                if (int.TryParse(value, out temp))
                {
                    window = temp;
                }

                return window;
            }
        }

        public static int MaximumPasswordAttempts
        {
            get
            {
                int attempts = 5, temp = 0;
                string value = Find("MaxPasswordAttempts");

                if (int.TryParse(value, out temp))
                {
                    attempts = temp;
                }

                return attempts;
            }
        }

        public static string Find(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? string.Empty;
        }
    }
}
