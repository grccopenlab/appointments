﻿using System;

namespace OpenLab.Common
{
    [Serializable]
    public class DuplicateUserNameException : Exception
    {
        public string UserName { get; private set; }

        public DuplicateUserNameException(string userName, string message = null)
            : base(string.IsNullOrWhiteSpace(message) ? string.Format("User name '{0}' has already been assigned.", userName) : message) 
        {
            this.UserName = userName;
        }
    }
}
