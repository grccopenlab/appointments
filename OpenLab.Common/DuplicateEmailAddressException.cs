﻿using System;

namespace OpenLab.Common
{
    [Serializable]
    public class DuplicateEmailAddressException : Exception
    {
        public string EmailAddress { get; private set; }

        public DuplicateEmailAddressException(string emailAddress, string message = null)
            : base(string.IsNullOrWhiteSpace(message) ? string.Format("Email address '{0}' has already been registered.", emailAddress) : message)
        {
            this.EmailAddress = emailAddress;
        }
    }
}
