﻿using System;

namespace OpenLab.Common
{
    [Serializable]
    public class NotAuthorizedForActionException : Exception
    {
        public string ActionAttempted { get; private set; }

        public NotAuthorizedForActionException(string actionAttempted, string message = null)
            :base(string.IsNullOrWhiteSpace(message) ? string.Format("An action was attempted that was not allowed: {0}", actionAttempted) : message) 
        {
            this.ActionAttempted = actionAttempted;
        }
    }
}
