﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Appointments.Data
{
    public partial class OpenLabContext : DbContext
    {
        public int SaveChanges(bool verboseLogging)
        {
            if (verboseLogging)
            {
                try
                {
                    return SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    var exceptionMessage = BuildVerboseExceptionMessage(ex);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
            else
            {
                return SaveChanges();
            }
        }

        public Task<int> SaveChangesAsync(bool verboseLogging)
        {
            if (verboseLogging)
            {
                try
                {
                    return SaveChangesAsync();
                }
                catch (DbEntityValidationException ex)
                {
                    var exceptionMessage = BuildVerboseExceptionMessage(ex);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
            else
            {
                return SaveChangesAsync();
            }
        }

        public Task<int> SaveChangesAsync(bool verboseLogging, CancellationToken cancellationToken)
        {
            if (verboseLogging)
            {
                try
                {
                    return SaveChangesAsync(cancellationToken);
                }
                catch (DbEntityValidationException ex)
                {
                    var exceptionMessage = BuildVerboseExceptionMessage(ex);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
            else
            {
                return SaveChangesAsync(cancellationToken);
            }
        }


        private string BuildVerboseExceptionMessage(DbEntityValidationException ex)
        {
            // Retrieve the error messages as a list of strings.
            var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => string.Format(" --> {0}", x.ErrorMessage));

            // Join the list to a single string.
            var fullErrorMessage = string.Join(Environment.NewLine, errorMessages);

            // Combine the original exception message with the new one.
            return string.Concat(ex.Message, Environment.NewLine, Environment.NewLine, "The validation errors are: ", Environment.NewLine, fullErrorMessage, Environment.NewLine);
        }
    }
}
