﻿using System;
using System.Collections.Generic;

namespace Appointments.Entities
{
    public interface IUserEntity
    {
        long ID { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string Salt { get; set; }
        string DisplayName { get; set; }
        bool IsActive { get; set; }
        string ResetToken { get; set; }
        bool IsLocked { get; set; }
        DateTime? LockExpires { get; set; }
        long LoginAttempts { get; set; }

        IEnumerable<ISimpleEntity> Roles { get; set; }
    }
}
