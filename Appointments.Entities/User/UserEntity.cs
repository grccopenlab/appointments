﻿using System;
using System.Collections.Generic;

namespace Appointments.Entities
{
    public class UserEntity : IUserEntity
    {
        public long ID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string DisplayName { get; set; }

        public bool IsActive { get; set; }

        public string ResetToken { get; set; }

        public bool IsLocked { get; set; }

        public DateTime? LockExpires { get; set; }

        public long LoginAttempts { get; set; }

        public IEnumerable<ISimpleEntity> Roles { get; set; }
    }
}
