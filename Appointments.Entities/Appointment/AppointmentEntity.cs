﻿using System;

namespace Appointments.Entities
{
    public class AppointmentEntity : IAppointmentEntity
    {
        public long ID { get; set; }
        public long StudentID { get; set; }
        public long TutorID { get; set; }
        public long SubjectID { get; set; }
        public DateTime ScheduledDate { get; set; }
        public bool IsComplete { get; set; }
        public bool IsCancelled { get; set; }
        public string CancelReason { get; set; }
        public long CancelledByUserID { get; set; }
        public bool WasMissed { get; set; }
        public string ReasonForAppointment { get; set; }
        public string AreaOrConcept { get; set; }
        public string TextPageReferences { get; set; }
        public bool NeedsHelpWithTextbook { get; set; }
        public bool NeedsHelpWithTerminology { get; set; }
        public bool NeedsHelpWithCoreConcepts { get; set; }
        public bool NeedsHelpWithCourseSoftware { get; set; }
        public string NeedsHelpOther { get; set; }
        public bool WereGoalsMet { get; set; }
        public bool WasProblemSolved { get; set; }
        public bool WasTutorHelpful { get; set; }
        public string Feedback { get; set; }

        public IUserEntity CancelledByUser { get; set; }
        public ISubjectEntity Subject { get; set; }
        public ITutorEntity Tutor { get; set; }
        public IStudentEntity Student { get; set; }
    }
}
