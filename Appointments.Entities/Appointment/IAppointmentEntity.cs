﻿using System;

namespace Appointments.Entities
{
    public interface IAppointmentEntity
    {
        long ID { get; set; }
        long StudentID { get; set; }
        long TutorID { get; set; }
        long SubjectID { get; set; }
        DateTime ScheduledDate { get; set; }
        bool IsComplete { get; set; }
        bool IsCancelled { get; set; }
        string CancelReason { get; set; }
        long CancelledByUserID { get; set; }
        bool WasMissed { get; set; }
        string ReasonForAppointment { get; set; }
        string AreaOrConcept { get; set; }
        string TextPageReferences { get; set; }
        bool NeedsHelpWithTextbook { get; set; }
        bool NeedsHelpWithTerminology { get; set; }
        bool NeedsHelpWithCoreConcepts { get; set; }
        bool NeedsHelpWithCourseSoftware { get; set; }
        string NeedsHelpOther { get; set; }
        bool WereGoalsMet { get; set; }
        bool WasProblemSolved { get; set; }
        bool WasTutorHelpful { get; set; }
        string Feedback { get; set; }

        IUserEntity CancelledByUser { get; set; }
        ISubjectEntity Subject { get; set; }
        ITutorEntity Tutor { get; set; }
        IStudentEntity Student { get; set; }
    }
}
