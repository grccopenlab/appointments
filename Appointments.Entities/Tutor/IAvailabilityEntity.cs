﻿namespace Appointments.Entities
{
    public interface IAvailabilityEntity
    {
        long ID { get; set; }
        long SemesterID { get; set; }
        int DayOfWeek { get; set; }
        int StartTime { get; set; }
        int EndTime { get; set; }
    }
}
