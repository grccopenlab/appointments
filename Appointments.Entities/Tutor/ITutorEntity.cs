﻿namespace Appointments.Entities
{
    public interface ITutorEntity
    {
        long ID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string EmailAddress { get; set; }
        string Phone { get; set; }
        bool IsActive { get; set; }
    }
}
