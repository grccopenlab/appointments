﻿namespace Appointments.Entities
{
    public interface ISkillsEntity
    {
        long ID { get; set; }
        long SubjectID { get; set; }
        long LevelID { get; set; }
    }
}
