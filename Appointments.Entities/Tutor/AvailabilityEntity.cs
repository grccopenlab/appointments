﻿namespace Appointments.Entities
{
    public class AvailabilityEntity : IAvailabilityEntity
    {
        public long ID { get; set; }
        public long SemesterID { get; set; }
        public int DayOfWeek { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
    }
}
