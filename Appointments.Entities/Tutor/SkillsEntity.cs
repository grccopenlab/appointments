﻿namespace Appointments.Entities
{
    public class SkillsEntity : ISkillsEntity
    {
        public long ID { get; set; }
        public long SubjectID { get; set; }
        public long LevelID { get; set; }
    }
}
