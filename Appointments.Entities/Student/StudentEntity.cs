﻿namespace Appointments.Entities
{
    public class StudentEntity : IStudentEntity
    {
        public long ID { get; set; }
        public string CampusID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
    }
}
