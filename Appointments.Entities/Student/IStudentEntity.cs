﻿namespace Appointments.Entities
{
    public interface IStudentEntity
    {
        long ID { get; set; }
        string CampusID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string EmailAddress { get; set; }
        string Phone { get; set; }
        bool IsActive { get; set; }
    }
}
