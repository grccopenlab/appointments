﻿namespace Appointments.Entities
{
    public interface ISimpleEntity
    {
        long ID { get; set; }
        string Name { get; set; }
    }
}
