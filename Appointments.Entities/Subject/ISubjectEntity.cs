﻿namespace Appointments.Entities
{
    public interface ISubjectEntity
    {
        long ID { get; set; }
        string Code { get; set; }
        string Name { get; set; }
        bool IsActive { get; set; }
    }
}
