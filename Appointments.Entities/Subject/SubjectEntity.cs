﻿namespace Appointments.Entities
{
    public class SubjectEntity : ISubjectEntity
    {
        public long ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
