﻿namespace Appointments.Entities
{
    public class InstructorEntity : IInstructorEntity
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsActive { get; set; }
    }
}
