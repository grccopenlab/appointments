﻿namespace Appointments.Entities
{
    public interface IInstructorEntity
    {
        long ID { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string EmailAddress { get; set; }
        bool IsActive { get; set; }
    }
}
