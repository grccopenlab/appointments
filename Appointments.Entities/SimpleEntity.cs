﻿namespace Appointments.Entities
{
    public class SimpleEntity : ISimpleEntity
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}
