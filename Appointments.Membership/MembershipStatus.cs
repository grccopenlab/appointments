﻿namespace Appointments.Membership
{
    public enum MembershipStatus
    {
        Unknown = 1,
        NotRegistered = 2,
        Active = 3,
        NotActive = 4,
        Locked = 5
    }
}