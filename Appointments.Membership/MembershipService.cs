﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using Appointments.Data;
using Appointments.Entities;
//
using OpenLab.Common;
using OpenLab.Crypto;

namespace Appointments.Membership
{
    public class MembershipService : IMembershipService
    {
        Data.OpenLabContext _context;

        Data.OpenLabContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new Data.OpenLabContext();
                }

                return _context;
            }
        }


        #region IDisposable Members

        bool disposed = false;

        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposeAll)
        {
            if (!disposed)
            {
                if (disposeAll)
                {
                    _context.Dispose();
                    _context = null;
                }

                disposed = true;
            }
        }

        #endregion


        public MembershipStatus GetMembershipStatus(string userName)
        {
            MembershipStatus status = MembershipStatus.NotRegistered;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)); ;

                if (user != null)
                {
                    if (user.IsActive)
                    {
                        if (!user.IsLocked)
                        {
                            if (user.LockExpires < DateTime.Now)
                            {
                                status = MembershipStatus.Active;
                            }
                            else
                            {
                                status = MembershipStatus.Locked;
                            }
                        }
                        else
                        {
                            status = MembershipStatus.Active;
                        }
                    }
                    else
                    {
                        status = MembershipStatus.NotActive;
                    }
                }
            }

            return status;
        }

        public AuthenticationResult AuthenticateUser(string userName, string password)
        {
            AuthenticationResult result = AuthenticationResult.InvalidUserName;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                if (!string.IsNullOrWhiteSpace(password))
                {
                    var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

                    if (user != null)
                    {
                        if (!user.IsActive)
                        {
                            result = AuthenticationResult.UserNotActive;
                        }
                        else
                        {
                            if (!user.IsLocked || user.LockExpires < DateTime.Now)
                            {
                                if (HashUtility.VerifyHashedPassword(user.Password, user.Salt + password))
                                {
                                    result = AuthenticationResult.Success;

                                    // TODO: Update IsLocked, LockExpires, and PasswordAttempts fields on this user
                                    UnlockUser(userName);
                                }
                                else
                                {
                                    if (user.LoginAttempts > AppSettings.MaximumPasswordAttempts)
                                    {
                                        result = AuthenticationResult.PasswordAttemptsExceeded;

                                        // TODO: Update IsLocked & LockExpires field on this user
                                        LockUser(userName);
                                    }
                                    else
                                    {
                                        result = AuthenticationResult.InvalidCredentials;

                                        // TODO: Update PasswordAttempts field on this user
                                        SetPasswordAttemptsForUser(userName, user.LoginAttempts + 1);
                                    }
                                }
                            }
                            else
                            {
                            }
                        }
                    }
                }
                else
                {
                    result = AuthenticationResult.InvalidPassword;
                }
            }

            return result;
        }

        public bool LockUser(string userName)
        {
            bool success = false;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
                if (user != null)
                {
                    user.IsLocked = true;
                    user.LockExpires = DateTime.Now.AddMinutes(AppSettings.LockOutWindow);

                    success = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                }
            }

            return success;
        }

        public bool UnlockUser(string userName)
        {
            bool success = false;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
                if (user != null)
                {
                    user.IsLocked = false;
                    user.LockExpires = null;
                    user.LoginAttempts = 0;

                    success = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                }
            }

            return success;
        }

        public bool SetPasswordAttemptsForUser(string userName, long attempts)
        {
            bool success = false;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
                if (user != null)
                {
                    user.LoginAttempts = attempts;

                    success = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                }
            }

            return success;
        }

        public bool IsUserNameAvailable(string userName)
        {
            bool isAvailable = false;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                isAvailable = !Context.Users.Any(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
            }

            return isAvailable;
        }

        public bool UpdateUserPassword(string userName, string currentPassword, string newPassword)
        {
            bool success = false;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                if (!string.IsNullOrWhiteSpace(currentPassword) && !string.IsNullOrWhiteSpace(newPassword))
                {
                    var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

                    if (user != null)
                    {
                        if (HashUtility.VerifyHashedPassword(user.Password, user.Salt + currentPassword))
                        {
                            string salt = HashUtility.GenerateSalt();

                            user.Salt = salt;
                            user.Password = HashUtility.HashPassword(salt + newPassword);
                            user.IsLocked = false;
                            user.LockExpires= null;
                            user.LoginAttempts = 0;

                            success = Context.SaveChanges() > 0 || !Context.ChangeTracker.HasChanges();
                        }
                    }
                }
            }

            return success;
        }

        public string[] GetRolesForUser(string userName)
        {
            string[] roles = { };

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)); ;

                if (user != null)
                {
                    roles = user.Roles.Select(r => r.Name).ToArray();
                }
            }

            return roles;
        }

        public bool IsUserInRole(string userName, params string[] roles)
        {
            bool isInRole = false;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)); ;

                if (user != null)
                {
                    isInRole = user.Roles.Any(r => roles.Contains(r.Name));
                }
            }

            return isInRole;
        }

        public string GetDisplayNameForUser(string userName)
        {
            string displayName = string.Empty;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var user = Context.Users.SingleOrDefault(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)); ;

                if (user != null)
                {
                    displayName = user.DisplayName;
                }
            }

            return displayName;
        }
    }
}
