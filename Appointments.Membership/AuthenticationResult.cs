﻿namespace Appointments.Membership
{
    public enum AuthenticationResult
    {
        Unknown = 1,
        Success = 2,
        InvalidUserName = 3,
        InvalidPassword = 4,
        InvalidCredentials = 5,
        PasswordAttemptsExceeded = 6,
        UserNotActive = 7
    }
}
