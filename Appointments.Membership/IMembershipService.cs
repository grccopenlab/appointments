﻿using System;

namespace Appointments.Membership
{
    public interface IMembershipService : IDisposable
    {
        MembershipStatus GetMembershipStatus(string userName);
        AuthenticationResult AuthenticateUser(string userName, string password);
        bool SetPasswordAttemptsForUser(string userName, long attempts);
        bool LockUser(string userName);
        bool UnlockUser(string userName);
        bool IsUserNameAvailable(string userName);
        bool UpdateUserPassword(string userName, string currentPassword, string newPassword);
        string[] GetRolesForUser(string userName);
        bool IsUserInRole(string userName, params string[] roles);
        string GetDisplayNameForUser(string userName);
    }
}
